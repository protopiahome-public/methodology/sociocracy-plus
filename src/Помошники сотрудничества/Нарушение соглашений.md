### Нарушение соглашений

Нарушайте соглашения, когда вы уверены, что выгода для сообщества перевешивает затраты на ожидание внесения поправок в соглашение, и берите на себя ответственность за любые последствия.

Хотя иногда соглашения необходимо нарушать, это может дорого обойтись для сообщества.

Вы несете ответственность за:

- Исправление возникших последствий
- Скорейшую работу с теми, кого это затронуло
- Изменение соглашения вместо неоднократных его нарушений
