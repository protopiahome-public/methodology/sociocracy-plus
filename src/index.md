# Технология группы 3. Сетевые сообщества и социократия плюс.

## О чем эта книга



Эта книга о информальных группах, как они организуются и как управляются. В книге будет расмотрен жизненный цикл таких группы, описаны основные примеры и предложены методы организации и управления такими группами.  Информальные группы сейчас настолько широко распространенны, что мы уже живем ими не можем представить, что всего 50 лет назад мир был устроен по другому. Термин информальная группа был введен русским историком Александром Шубином. Эта книга о креативных горизонтально информационно-неформальных группах.



Книга имеет двойное название. С одной стороны она продолжает линейку "Технологии группы", с другой стороны сильно опирается на методики горизонтального управления "Социократия 3.0". Тем самым она нацелена не просто дать полезные знания, а именно дать методы с помощью которых  можно создавать информальные группы.